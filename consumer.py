# amqps://luntpnxt:X2wuIZ6WJoiSgQ3mpvFWtyHb3GS4yfca@rattlesnake.rmq.cloudamqp.com/luntpnxt
import pika, json, os,django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "admin.settings")
django.setup()

from products.models import Product

params = pika.URLParameters("amqps://luntpnxt:X2wuIZ6WJoiSgQ3mpvFWtyHb3GS4yfca@rattlesnake.rmq.cloudamqp.com/luntpnxt")
connection = pika.BlockingConnection(params)
channel = connection.channel()

channel.queue_declare(queue='admin')

def callback(ch, method, properties, body):
    print("Received in admin")
    id = json.loads(body)
    print(id)

    product = Product.objects.get(id=id)
    product.likes += 1
    product.save()
    print("product {0} has one more like ! total {1}".format(product.title, product.likes))

channel.basic_consume(queue='admin', on_message_callback=callback, auto_ack=True)

print("started consuming")

channel.start_consuming()

channel.close()
